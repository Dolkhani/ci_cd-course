from python:3.6.9-alpine

COPY . /srv/app
CMD python /srv/app/hello.py

EXPOSE 8080
